﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LS.Flex;

namespace LS.Flexer
{
    class Program
    {
        static void Main(string[] args)
        {
            var p = new Parser(File.ReadAllText(args.Length == 1 ? args[0] : @"C:\Users\fast\Documents\flex.cs"));
            p.Enviroment = new string[] { "LS.Flexer.Permissions" };
            p.Api.Alias("grant", "LS.Flexer.Permissions:Grant");
            p.Api.Print = x =>
            {
                Console.Write("[Flexer.Out]: ");
                Console.WriteLine(x);
            };
            p.Api.Read = () =>
            {
                Console.Write("[Flexer.In]: ");
                return Console.ReadLine();
            };
            p.OnInvocation += (sender, e) =>
            {
                if ((e.Method?.Name == nameof(Console.WriteLine) || e.Method?.Name == nameof(Console.Write)) && e.Type == typeof(Console))
                {
                    Console.Write("[Flexer.Out]: ");
                    return;
                }
                if ((e.Method?.Name == nameof(Console.ReadLine) || e.Method?.Name == nameof(Console.ReadKey) || e.Method?.Name == nameof(Console.Read)) && e.Type == typeof(Console))
                {
                    Console.Write("[Flexer.In]: ");
                    return;
                }
                if (e.Type.FullName.StartsWith("LS.Flex.") 
                || Permissions.Perms.Any(perm => perm.EndsWith(".*") ? 
                    e.Type.FullName.StartsWith(perm.Substring(0, perm.Length-2)) && e.Type.FullName.Count(x=> x=='.') == perm.Count(x=> x=='.') 
                    : e.Type.FullName == perm ) 
                || e.Target != null || e.Constructor != null)
                    return;
                if (e.Type == typeof(Permissions) && e.Name == nameof(Permissions.Grant))
                    return;
#if DEBUG
                return;
#endif
                e.Abort = true;
                Console.WriteLine("[Flexer.Security]: Нет разрешения на выполнения операций с типом {0}", e.Type.FullName);
            };
            if(false)
            {
                p.Run();
                Console.Read();
                return;
            }
            try
            {
                Console.WriteLine("[Flexer]: Выполнение flex скрипта");
                p.Run();
                Console.WriteLine("[Flexer]: Конец flex скрипта");
            }
            catch (LS.Flex.Exceptions.RuntimeException ex)
            {
                Console.WriteLine("[Flexer.Debugger]: Ошибка");
                handle(ex);
                Console.WriteLine("[Flexer.Debugger]: Конец ошибки");
            }
        }
        static void handle(Exception ex)
        {
            Console.WriteLine(ex.Message);
            if (ex.InnerException != null)
                handle(ex.InnerException);
        }
    }
    public static class Permissions
    {
        public static IEnumerable<string> Perms => _perms.AsEnumerable();
        private readonly static List<string> _perms = new List<string>();
        public static void Grant(params string[] perms)
        {
            Console.WriteLine("[Flexer.Security]: Скрипт запрашивает разрешения на выполнение операций со следующими типами: {0}",
                string.Join(", ", perms.Select(x => $"\"{x}\"")));
            Console.Write("[Flexer.Security]: Для разрешения нажмите Enter ");
            while (Console.ReadKey().Key != ConsoleKey.Enter) ;
            Console.WriteLine();
            _perms.AddRange(perms.Where(x => x != "Flexer.Permissions"));
        }
    }
}