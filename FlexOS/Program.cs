﻿using LS.Flex;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlexOS
{
    static class Program
    {
        static Parser OS { get; } = new Parser(File.ReadAllText(@"C:\Users\fast\Documents\flex.cs"));
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            OS.Api.Print = Console.WriteLine;
            OS.Run();
            string cmd = null;
            OS.Api.Read = () => cmd;
            while (true)
            {
                cmd = Console.ReadLine();                
            }
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Main());
        }
    }
}
