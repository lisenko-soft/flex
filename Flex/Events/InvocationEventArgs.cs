﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace LS.Flex.Events
{
    public class InvocationEventArgs
    {
        public InvocationEventArgs(PropertyInfo property)
        {
            Property = property ?? throw new ArgumentNullException(nameof(property));
        }

        public InvocationEventArgs(MethodBase method)
        {
            Method = method ?? throw new ArgumentNullException(nameof(method));
        }

        public InvocationEventArgs(FieldInfo field)
        {
            Field = field ?? throw new ArgumentNullException(nameof(field));
        }

        public InvocationEventArgs(Type constructor)
        {
            Constructor = constructor ?? throw new ArgumentNullException(nameof(constructor));
        }

        public bool Abort { get; set; }

        public MethodBase Method { get; set; }
        public PropertyInfo Property { get; set; }
        public FieldInfo Field { get; set; }
        public Type Constructor { get; }

        public object Target { get; set; }
        public object[] Arguments { get; set; }

        public Type Type => Method?.DeclaringType ?? Property?.DeclaringType ?? Field?.DeclaringType ?? Constructor;
        public string Name => Method?.Name ?? Property?.Name ?? Field?.Name ?? Constructor.Name;
    }
}
