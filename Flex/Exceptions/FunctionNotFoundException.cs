﻿using System;
using System.Linq;

namespace LS.Flex.Exceptions
{
    public class FunctionNotFoundException : Exception
    {
        public FunctionNotFoundException(string typeName, string functionName, object[] args)
            : base($"Тип \"{typeName}\" не содержит функцию \"{functionName}\" " +
                  $"{string.Join(", ", args?.Select(x=> $"\"{x?.GetType().FullName ?? "NULL"}\"") ?? new string[0])}")
        {
            TypeName = typeName;
            FunctionName = functionName;
        }

        public string TypeName { get; set; }
        public string FunctionName { get; set; }
    }
}
