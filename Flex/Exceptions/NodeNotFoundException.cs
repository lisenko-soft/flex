﻿using System;

namespace LS.Flex.Exceptions
{
    public class NodeNotFoundException : Exception
    {
        public NodeNotFoundException() : base("Обрыв цепочки вызова")
        {
        }
    }
}
