﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LS.Flex.Exceptions
{
    public class InvocationException : Exception
    {
        public InvocationException(string typeName, string functionName, Exception innerException) : base($"Ошибка выполнения функции \"{functionName}\" типа \"{typeName}\". Подробности во внутреннем исключении.", innerException)
        {
            TypeName = typeName;
            FunctionName = functionName;
        }

        public string TypeName { get; set; }
        public string FunctionName { get; set; }
    }
}
