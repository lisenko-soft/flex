﻿using System;

namespace LS.Flex.Exceptions
{
    public class NameNotFoundException : Exception
    {
        public NameNotFoundException(string name) : base($"Имя \"{name}\" не существует в текущем контексте")
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}
