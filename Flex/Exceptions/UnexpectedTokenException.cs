﻿using System;

namespace LS.Flex.Exceptions
{
    public class UnexpectedTokenException : Exception
    {
        public UnexpectedTokenException(string token) : base($"Неизвестный токен \"{token}\"")
        {
            Token = token;
        }

        public string Token { get; set; }
    }
}
