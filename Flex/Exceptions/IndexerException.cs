﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LS.Flex.Exceptions
{
    public class IndexerException : Exception
    {
        public IndexerException(string typeName) : base($"Недопустимая индексация к типу \"{typeName}\"")
        {
            TypeName = typeName;
        }

        public string TypeName { get; set; }
    }
}
