﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LS.Flex.Exceptions
{
    public class RuntimeException : Exception
    {
        public RuntimeException(Node node, Exception innerException) : base($"Ошибка выполнения Flex скрипта строка {(node?.Number.ToString()??"NaN")}. Подробности во внутреннем исключении.", innerException)
        {            

        }
    }
}