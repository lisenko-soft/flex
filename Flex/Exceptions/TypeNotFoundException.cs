﻿using System;

namespace LS.Flex.Exceptions
{
    public class TypeNotFoundException : Exception
    {
        public TypeNotFoundException(string typeName) : base($"Не удалось найти тип \"{typeName}\"")
        {
            TypeName = typeName;
        }

        public string TypeName { get; set; }
    }
}
