﻿using System;

namespace LS.Flex.Breaks
{
    public interface IBreak
    {
        Exception Break { get; }
    }
}
