﻿using System;

namespace LS.Flex.Breaks
{
    public class BreakBreak : Exception, IBreak
    {
        public Exception Break => this;
    }
}
