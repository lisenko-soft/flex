﻿using System;

namespace LS.Flex.Breaks
{
    public class ReturnBreak : Exception, IBreak
    {
        public ReturnBreak(bool isVoid, object result)
        {
            Result = result;
        }

        public object Result { get; set; }

        public Exception Break => this;
    }
}
