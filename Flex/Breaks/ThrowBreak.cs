﻿using System;

namespace LS.Flex.Breaks
{
    public class ThrowBreak : Exception, IBreak
    {
        public ThrowBreak(Exception ex)
        {
            Break = ex;
        }

        public Exception Break { get; }
    }
}
