﻿using System.Linq;

namespace LS.Flex.Syntax
{
    public static partial class Math
    {
        #region -- System.String --
        public static string Addition(params string[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res += obj;
            return res;
        }
        #endregion

        #region -- System.Int32 --
        public static int Addition(params int[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res += obj;
            return res;
        }
        public static int Subtraction(params int[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res -= obj;
            return res;
        }
        public static int Multiplication(params int[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res *= obj;
            return res;
        }
        public static int Division(params int[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res /= obj;
            return res;
        }
        #endregion

        #region -- System.Float --
        public static float Addition(params float[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res += obj;
            return res;
        }
        public static float Subtraction(params float[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res -= obj;
            return res;
        }
        public static float Multiplication(params float[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res *= obj;
            return res;
        }
        public static float Division(params float[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res /= obj;
            return res;
        }
        #endregion

        #region -- System.Double --
        public static double Addition(params double[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res += obj;
            return res;
        }
        public static double Subtraction(params double[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res -= obj;
            return res;
        }
        public static double Multiplication(params double[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res *= obj;
            return res;
        }
        public static double Division(params double[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res /= obj;
            return res;
        }
        #endregion

        #region -- System.Decimal --
        public static decimal Addition(params decimal[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res += obj;
            return res;
        }
        public static decimal Subtraction(params decimal[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res -= obj;
            return res;
        }
        public static decimal Multiplication(params decimal[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res *= obj;
            return res;
        }
        public static decimal Division(params decimal[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res /= obj;
            return res;
        }
        #endregion

        #region -- System.Int64 --
        public static long Addition(params long[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res += obj;
            return res;
        }
        public static long Subtraction(params long[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res -= obj;
            return res;
        }
        public static long Multiplication(params long[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res *= obj;
            return res;
        }
        public static long Division(params long[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res /= obj;
            return res;
        }
        #endregion

        #region -- System.Int16 --
        public static short Addition(params short[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res += obj;
            return res;
        }
        public static short Subtraction(params short[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res -= obj;
            return res;
        }
        public static short Multiplication(params short[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res *= obj;
            return res;
        }
        public static short Division(params short[] objs)
        {
            var res = objs[0];
            foreach (var obj in objs.Skip(1))
                res /= obj;
            return res;
        }
        #endregion
    }
}
