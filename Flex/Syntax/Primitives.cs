﻿using System.Linq;
using System.Text.RegularExpressions;

namespace LS.Flex.Syntax
{
    public static class Primitives
    {
        public static string String(string str)
        {
            return Regex.Unescape(str);
        }
        public static string Decimal(string dec)
        {
            return dec.Replace(".", ",");
        }
        public static string Char(string ch)
        {
            if (ch.Last() == '\'' && ch.First() == '\'' && ch.Length > 2)
                return String(ch).Replace("'", string.Empty);
            else
                return null;
        }
    }
}
