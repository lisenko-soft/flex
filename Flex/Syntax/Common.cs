﻿using LS.Flex.Breaks;
using LS.Flex.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace LS.Flex.Syntax
{
    public static class Common
    {
        public static Type TypeOf(Type type) => type;
        public static Type TypeOf(string type) => TypeUtils.FindType(type);
        public static object Cast(Type type, object data)
        {
            var DataParam = Expression.Parameter(typeof(object), "data");
            var Body = Expression.Block(Expression.Convert(Expression.Convert(DataParam, data.GetType()), type));
            var Run = Expression.Lambda(Body, DataParam).Compile();
            var ret = Run.DynamicInvoke(data);
            return ret;
        }
        internal static readonly IReadOnlyDictionary<string, string> const_alias = new Dictionary<string, string>
        {
            { "+", "LS.Flex.Syntax.Math:Addition" },
            { "-", "LS.Flex.Syntax.Math:Subtraction" },
            { "*", "LS.Flex.Syntax.Math:Multiplication" },
            { "/", "LS.Flex.Syntax.Math:Division" },
            { "if", "LS.Flex.Syntax.Logic:If" },
            { "foreach", "LS.Flex.Syntax.Logic:Foreach" },
            { "sleep", "System.Threading.Thread:Sleep" },
            { "typeof", "LS.Flex.Syntax.Common:TypeOf" },
            { "return", "LS.Flex.Syntax.Common:Return" },
            { "throw", "LS.Flex.Syntax.Common:Throw" },
            { "cast", "LS.Flex.Syntax.Common:Cast" },
            { "print", "this:Print:Invoke" },
            { "read", "this:Read:Invoke" },
            { "use", "this:Import" },
            { "alias", "this:Alias" },
            { "env", "this:Environment" },
            { "array", "System.Array:CreateInstance" },
            { "object", typeof(object).FullName },
            { "bool", typeof(bool).FullName },
            { "int", typeof(int).FullName },
            { "short", typeof(short).FullName },
            { "long", typeof(long).FullName },
            { "byte", typeof(byte).FullName },
            { "float", typeof(float).FullName },
            { "uint", typeof(uint).FullName },
            { "ulong", typeof(ulong).FullName },
            { "ushort", typeof(ushort).FullName },
            { "string", typeof(string).FullName },
            { "char", typeof(char).FullName },
            { "double", typeof(double).FullName }
        };
        internal static string Allias(string s)
        {
            if (!const_alias.ContainsKey(s))
                return s;
            var a = const_alias[s];
            if (a.Contains(":"))
                return s;
            return a;
        }
        internal static Node Allias(string s, Node caller)
        {
            string pref = string.Empty;
            if (s.First() == '=')
            {
                pref = "=";
                s = s.Substring(1);
            }
            if (s.First() == '`')
            {
                pref = "`";
                s = s.Substring(1);
            }
            //if (s == "read")
            //{
            //    var a = 2;
            //}
            if (const_alias.ContainsKey(s))
            {
                var a = const_alias[s];
                var parts = a.Split(':');
                var nodes = new List<Node> {new Node(new NodeInfo
                {
                    Level = caller.Level,
                    Line = pref + parts.First(),
                    Number = caller.Number
                })};
                nodes.Last().Parrent = caller.Parrent;
                foreach (var current in parts.Skip(1).Select(part => new Node(new NodeInfo
                {
                    Level = caller.Level,
                    Line = ':' + part,
                    Number = caller.Number
                })))
                {
                    current.Parrent = nodes.Last();
                    nodes.Last().NextNode = current;
                    nodes.Add(current);
                }
                if (parts.Length > 1)
                {
                    (nodes.Last().Nodes as List<Node>).AddRange(caller.Nodes.Each(x => x.Parrent = nodes.Last()));
                }
                if (caller.Parrent.NextNode == caller)
                {
                    caller.Parrent.LastNode = nodes.First();
                }
                nodes.Last().NextNode = caller.NextNode;
                return nodes.First();
            }
            return null;
        }
        public static void Return(params object[] result) => throw new ReturnBreak(!result.Any(), result.FirstOrDefault());
        public static void Throw(Exception ex) => throw new ThrowBreak(ex);
    }
}
