﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace LS.Flex.Syntax
{
    public static class Logic
    {
        public static void If(bool @if, Action then)
        {
            if (@if)
                then();
        }
        public static void If(bool @if, Action then, Action @else)
        {
            if (@if)
                then();
            else
                @else();
        }
        public static void If(bool? @if, Action then) => If(@if ?? false, then);
        public static void If(bool? @if, Action then, Action @else) => If(@if ?? false, then, @else);
        public static void Foreach(IEnumerable vs, Action<object> action)
        {
            foreach(var obj in vs)
            {
                action(obj);
            }
        }
        public static void Foreach(IEnumerable vs, Action<object, int> action)
        {
            int i = 0;
            foreach (var obj in vs)
            {
                action(obj, i);
                i++;
            }
        }
    }
}
