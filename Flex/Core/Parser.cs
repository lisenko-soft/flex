﻿using LS.Flex.Events;
using LS.Flex.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace LS.Flex
{
    public class Parser : Node
    {
        public Parser(string code)
        {
            Api = new Api(this);
            Parrent = this;
            Vars = new Dictionary<string, object>();
            var content = Regex.Replace(code, "\\\"(.*?)[^\\\\]\\\"", new MatchEvaluator(Match));
            var lines = content.Split('\n').Select(x => x.Split(new string[] { "//" }, StringSplitOptions.None).First().TrimEnd()).ToList();
            string separator = new string(' ', 4);
            if (lines.First().Contains(":"))
            {
                var ver = lines.First().Split(':').Last();
                separator = lines.First().Split(':').First();
            }
            foreach (string line in lines.Where(x => x != ""))
            {
                if (line.Length > 4 && line.Substring(0, 4) == "use ")
                {
                    Namespaces.Add(line.Substring(4, line.Length - 4));
                    continue;
                }
                var newline = line.Replace(separator, "");
                var n = new Node(new NodeInfo
                {
                    Level = (line.Length - newline.Length) / separator.Length,
                    Line = newline,
                    Number = lines.IndexOf(line) + 1
                });
                add(n, this);
            }
        }
        private const string childsymb = ":`=";
        internal string Separator { get; private set; }
        public event InvocationEvent OnInvocation;
        internal void InvokeEvent(Node n, InvocationEventArgs e)
        {
            if (OnInvocation != null && n.Parser == this)
                OnInvocation(n, e);
        }
        private void add(Node node, Node to)
        {
            if (node.Level == to.Level)
            {
                node.Parrent = to;
                to.NextNode = node;
            }
            else if (node.Level == to.Level + 1)
            {
                if (!childsymb.Contains(node.Line.First()))
                {
                    node.Parrent = to;
                    to.Nodes.Add(node);
                }
                else
                {
                    if (to.NextNode != null)
                        add(node, to.NextNode);
                    if (to.Nodes.Any())
                        add(node, to.Nodes.Last());
                }
            }
            else
            {
                if (to.Nodes.Any())
                    add(node, to.Nodes.Last().LastNode);
            }
        }
        internal Type FindType(string name)
        {
            Type t = TypeUtils.FindType(name);
            if (t != null)
                return t;
            foreach (var space in Namespaces)
            {
                t = TypeUtils.FindType(space + '.' + name);
                if (t != null)
                    return t;
            }
            return t;
        }
        public Api Api { get; }
        public string[] Enviroment { get; set; } = new string[0];
        ICollection<string> Namespaces { get; } = new List<string>();
        IDictionary<string, string> Strings { get; } = new Dictionary<string, string>();
        internal string String(string str) => Strings[str];
        public new void Run() => base.Run();
        string Match(Match match)
        {
            Strings["\"" + Strings.Count + "\""] = match.Value.Substring(1, match.Length - 2);
            return Strings.Keys.Last();
        }
    }
}
