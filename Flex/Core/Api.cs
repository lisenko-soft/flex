﻿using LS.Flex.Exceptions;
using LS.Flex.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LS.Flex
{
    public class Api
    {
        private readonly Parser parser;
        internal Api(Parser parser)
        {
            this.parser = parser ?? throw new ArgumentNullException(nameof(parser));
        }
        public bool Alias(string alias, string full)
        {
            if (Common.const_alias.ContainsKey(alias))
                return false;
            (Common.const_alias as IDictionary<string, string>)[alias] = full;
            return true;
        }
        public void Import(params string[] dlls)
        {
            foreach (var dll in dlls)
                Assembly.LoadFrom(dll);
        }
        public bool Environment(string env) => parser.Enviroment.Concat(new[] { "LS.Flex.Environments" }).Contains(env);
        public Action<object> Print { get; set; }
        public Func<object> Read { get; set; }
    }
}
