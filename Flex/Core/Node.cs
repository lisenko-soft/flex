﻿using LS.Flex.Breaks;
using LS.Flex.Events;
using LS.Flex.Exceptions;
using LS.Flex.Syntax;
using LS.Flex.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;

namespace LS.Flex
{
    public class Node
    {
        private Node _parrent;
        private Node _childNode;

        internal Parser Parser => Parrent as Parser ?? Parrent.Parser;
        internal Node Scope => Parrent.Vars != null ? Parrent : Parrent.Scope;
        internal Node Parrent
        {
            get => _parrent;
            set
            {
                if (_parrent == null)
                    _parrent = value;
            }
        }
        internal string Line { get; private set; }
        internal int Level { get; private set; } = -1;
        internal int Number { get; private set; } = -1;
        /// <summary>
        /// Цепочка вызовов 
        /// </summary>
        internal Node NextNode { get => _childNode; set { if (_childNode == null) _childNode = value; else _childNode.NextNode = value; } }
        internal Node LastNode { get => NextNode?.LastNode ?? this; set => _childNode = value; }
        /// <summary>
        /// Аргументы
        /// </summary>
        internal ICollection<Node> Nodes { get; } = new List<Node>();
        internal Dictionary<string, object> Vars { get; set; }
        protected Node() { }
        internal Node(NodeInfo info)
        {
            Line = info.Line;
            Number = info.Number;
            Level = info.Level;
        }
        internal void Debug(string s)
        {
            Console.WriteLine(s + this);
            foreach (var c in Nodes)
                c.Debug(s + "-");
            NextNode?.Debug(">" + s);
        }
        internal bool isSet => Line.TrimStart().First() == '=';
        internal bool isIndex => Line.TrimStart().First() == '`';
        private object next(object val, Action<object> set)
        {
            if (NextNode?.isSet ?? true)
            {
                set?.Invoke(val);
                return val;
            }
            return NextNode.Invoke(val, set: set);
        }
        internal object Run()
        {
            try
            {
                foreach (var node in Nodes)
                    node.Invoke(null);
            }
            catch (ReturnBreak brake)
            {
                return brake.Result;
            }
            return null;
        }
        internal object Invoke(object sender, Type parrentType = null, Action<object> set = null)
        {
            string line = Line.Trim();
            var alias = Common.Allias(line, this);
            if (alias != null)
                return alias.Invoke(sender, parrentType, set);
            Type type = parrentType;
            if (type == null)
                type = sender?.GetType();
            if (isSet)
                line = line.Substring(1);
            if (isIndex && sender == null)
                line = line.Substring(1);
            if (line == "null")
                return null;
            if (line == "this")
                return next(Parser.Api, set);
            if (line == "self") //chto eto?
            {

            }
            if (line.First() == '$')
            {
                var global = line.ElementAt(1) == '$';
                Dictionary<string, object> mem = global ? Parser.Vars : Scope.Vars;
                var name = line.Substring(1);
                if (NextNode?.isSet == true)
                    return NextNode.Invoke(null, set: o => mem[name] = o);
                if (!mem.ContainsKey(name))
                    throw new RuntimeException(this, new NameNotFoundException(name));
                var obj = mem[name];
                //if (obj is Delegate)
                //{
                //    if (NextNode?.Line.Trim() == ":")
                //    {
                //        set?.Invoke(obj);
                //        return null;
                //    }
                //    var args = Nodes.Select(x => x.Invoke(null)).ToArray();
                //    return next(((Delegate)obj).DynamicInvoke(args), set);
                //}
                return next(obj, set);
            }
            if (line.First() == '\\')
            {
                Type retype = Parser.FindType(Common.Allias(line.Substring(1).Trim()));
                Dictionary<string, Type> args = Nodes.Where(x => x.Line.First() == '$').ToDictionary(x => x.Line.Substring(1).Split(':').First().Trim(),
                    x => x.Line.Contains(":") ? Parser.FindType(Common.Allias(x.Line.Split(':').Last().Trim())) : typeof(object));
                var lambdaPar = args.Select(x => Expression.Parameter(x.Value, x.Key)).ToArray();
                var list = new StupidList<object>();
                var block = lambdaPar.Select(prm => Expression.Call(Expression.Constant(list),
                list.GetType().GetMethod(nameof(list.StupidAdd)).MakeGenericMethod(prm.Type), prm) as Expression).ToList();
                if (retype == null)
                {
                    Action act = () =>
                    {
                        NextNode.Vars = lambdaPar.Select((x, index) => new KeyValuePair<string, object>(x.Name, list[index]))
                        .ToDictionary(x => x.Key, x => x.Value);
                        list.Clear();
                        NextNode.Run();
                    };
                    block.Add(Expression.Invoke((Expression<Action>)(() => act())));
                }
                else
                {
                    Func<object> func = () =>
                    {
                        NextNode.Vars = lambdaPar.Select((x, index) => new KeyValuePair<string, object>(x.Name, list[index]))
                        .ToDictionary(x => x.Key, x => x.Value);
                        list.Clear();
                        return NextNode.Run();
                    };
                    block.Add(Expression.Convert(Expression.Invoke((Expression<Func<object>>)(() => func())), retype));
                }
                var lambda = Expression.Lambda(Expression.Block(block), lambdaPar).Compile();
                object deleg;
                if (retype == null)
                    deleg = Common.Cast(TypeUtils.MakeGeneric("System.Action", args.Select(x => x.Value).ToArray()), lambda);
                else
                    deleg = Common.Cast(TypeUtils.MakeGeneric("System.Func", args.Select(x => x.Value).Concat(new Type[] { retype }).ToArray()), lambda);
                return NextNode.next(deleg, set);
            }
            if (line == "\"\"")
                return next(string.Empty, set);
            var ch = Primitives.Char(line);
            if (ch != null)
                return next(char.Parse(Primitives.Char(line)), set);
            if (line.First() == '"' && line.Last() == '"')
                return next(string.Format(Primitives.String(Parser.String(line)), Nodes.Select(x => x.Invoke(null)).ToArray()), set);
            bool boo;
            if (bool.TryParse(line, out boo))
                return next(boo, set);
            var num = Primitives.Decimal(line);
            decimal dec;
            if (num.Contains(',') && decimal.TryParse(num, out dec))
                return next(dec, set);
            int i;
            if (int.TryParse(num, out i))
                return next(i, set);
            byte b;
            num = num.Substring(0, num.Length - 1);
            if (line.Last() == 'b' && byte.TryParse(num, out b))
                return next(b, set);
            short s;
            if (line.Last() == 's' && short.TryParse(num, out s))
                return next(s, set);
            float f;
            if (line.Last() == 'f' && float.TryParse(num, out f))
                return next(f, set);
            double d;
            if (line.Last() == 'd' && double.TryParse(num, out d))
                return next(d, set);
            long l;
            if (line.Last() == 'l' && long.TryParse(num, out l))
                return next(l, set);
            if (sender == null && type == null)
            {
                var t = Parser.FindType(line);
                if (t == null)
                    throw new RuntimeException(this, new TypeNotFoundException(line));
                if (NextNode == null)
                    return t;//throw new RuntimeException(this, new NodeNotFoundException());
                return NextNode.Invoke(null, t, set);
            }
            if (isIndex)
            {
                try
                {
                    var list = (sender as IList);
                    if (list == null)
                        throw new IndexerException(type.FullName);
                    var temp = NextNode;
                    LastNode = null;
                    object indexer = Nodes.Any() ? Nodes.First().Invoke(null) : Invoke(null);
                    NextNode = temp;
                    object result = NextNode?.isSet == true ? NextNode.Invoke(null) : null;
                    try
                    {
                        if (NextNode?.isSet == true)
                        {
                            list[(int)indexer] = result;
                            return result;
                        }
                        return next(list[(int)indexer], set);
                    }
                    catch (Exception ex) { throw new InvocationException(type.FullName, "`", ex); }
                }
                catch (Exception ex)
                {
                    throw new RuntimeException(this, ex);
                }
            }
            if (line.First() == ':')
            {
                var naming = line.Substring(1);
                var field = type.GetFields().SingleOrDefault(x => x.Name == naming);
                if (field != null)
                {
                    if (NextNode?.isSet == true)
                        return NextNode.Invoke(null, set: o => field.SetValue(sender, o));
                    object obj;
                    try
                    {
                        var args = new InvocationEventArgs(field);
                        args.Target = sender;
                        Parser.InvokeEvent(this, args);
                        if (args.Abort)
                            return null;
                        obj = field.GetValue(sender);
                    }
                    catch (Exception ex)
                    {
                        throw new RuntimeException(this, new InvocationException(type.FullName, naming, ex));
                    }
                    return next(obj, set);
                }
                var property = type.GetProperties().SingleOrDefault(x => x.Name == naming && x.CanRead);
                if (property != null)
                {
                    if (NextNode?.isSet == true)
                        return NextNode.Invoke(null, set: o => property.SetValue(sender, o));
                    object obj;
                    try
                    {
                        var args = new InvocationEventArgs(property);
                        args.Target = sender;
                        Parser.InvokeEvent(this, args);
                        if (args.Abort)
                            return null;
                        obj = property.GetValue(sender);
                    }
                    catch (Exception ex)
                    {
                        throw new RuntimeException(this, new InvocationException(type.FullName, naming, ex));
                    }
                    return next(obj, set);
                }
                IEnumerable<MethodBase> methods;
                if (naming == string.Empty && sender != null)
                    naming = nameof(Invoke);
                if (naming == string.Empty || naming == type.Name)
                    methods = type.GetConstructors().Concat(new ConstructorInfo[] { new ActivatorConstructor(type) });
                else
                    methods = type.GetMethods().Where(x => x.Name == naming);
                if (methods.Any())
                {
                    var args = Nodes.Select(x => x.Invoke(null)).ToArray();
                    foreach (var method in methods.Where(x => !x.ContainsGenericParameters).OrderBy(x => x.GetParameters().Length))
                    {
                        var margs = method.GetParameters();
                        bool ok = true;
                        for (int j = 0; j < args.Length; j++)
                        {
                            var arg = args[j];
                            ParameterInfo marg;
                            if (j >= margs.Length && (margs.LastOrDefault()?.IsParams() == true))
                                marg = margs.Last();
                            else if (margs.Length > j)
                                marg = margs[j];
                            else
                            {
                                ok = false;
                                break;
                            }
                            if (arg != null && !arg.GetType().MatchType(marg.IsParams() ? marg.ParameterType.GetElementType() : marg.ParameterType))
                            {
                                ok = false;
                                break;
                            }
                        }
                        if (!ok)
                            continue;
                        if (NextNode?.Line.Trim() == ":" && method is MethodInfo)
                        {
                            var deleg = TypeUtils.BuildDelegate((MethodInfo)method);
                            set?.Invoke(deleg);
                            return null;
                        }
                        if (margs.LastOrDefault()?.IsParams() == true)
                        {
                            var prms = args.Skip(margs.Length - 1).ToArray();
                            var arr = Array.CreateInstance(margs.Last().ParameterType.GetElementType(), prms.Length);
                            Array.Copy(prms, arr, arr.Length);
                            args = args.Take(margs.Length - 1).Concat(new object[] { arr }).ToArray();
                        }
                        object obj;
                        try
                        {
                            var eargs = method is ConstructorInfo ? new InvocationEventArgs(type) : new InvocationEventArgs(method);
                            eargs.Target = sender;
                            eargs.Arguments = args.ToArray();
                            Parser.InvokeEvent(this, eargs);
                            if (eargs.Abort)
                                return null;
                            if (method is ConstructorInfo)
                                obj = ((ConstructorInfo)method).Invoke(args);
                            else
                                obj = method.Invoke(sender, args);
                        }
                        catch (Exception ex)
                        {
                            if (ex.InnerException is IBreak)
                                throw (ex.InnerException as IBreak).Break;
                            throw new RuntimeException(this, new InvocationException(type.FullName, naming, ex));
                        }
                        return next(obj, set);
                    }
                    throw new RuntimeException(this, new FunctionNotFoundException(type.FullName, naming, args));
                }
                throw new RuntimeException(this, new FunctionNotFoundException(type.FullName, naming, null));
            }
            throw new RuntimeException(this, new UnexpectedTokenException(line));
        }
        public override string ToString()
        {
            return string.Format("Node#{0}: {1}", Level, Line);
        }
    }

}
