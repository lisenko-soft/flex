﻿namespace LS.Flex
{
    internal struct NodeInfo
    {
        internal string Line { get; set; }
        internal int Level { get; set; }
        internal int Number { get; set; }
    }

}
