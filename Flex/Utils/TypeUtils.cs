﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LS.Flex.Utils
{
    public static class TypeUtils
    {
        /// <summary>
        /// Looks in all loaded assemblies for the given type.
        /// </summary>
        /// <param name="fullName">
        /// The full name of the type.
        /// </param>
        /// <returns>
        /// The <see cref="Type"/> found; null if not found.
        /// </returns>
        public static Type FindType(string fullName)
        {
            var tt = Type.GetType(fullName);
            if (tt != null)
                return tt;
            return
                AppDomain.CurrentDomain.GetAssemblies()
                    .Where(a => !a.IsDynamic)
                    .SelectMany(a => a.GetTypes())
                    .FirstOrDefault(t => t.FullName.Equals(fullName));
        }
        public static bool IsParams(this ParameterInfo param)
        {
            return param.GetCustomAttributes(typeof(ParamArrayAttribute), false).Length > 0;
        }
        public static bool MatchType(this Type child, Type parrent)
        {
            if (child == parrent)
                return true;
            if (parrent.IsInterface && child.GetInterface(parrent.Name) != null)
                return true;
            if (child.BaseType == parrent)
                return true;
            if (child.BaseType != null)
                return MatchType(child.BaseType, parrent);
            return false;
        }
        public static IList<MethodInfo> GetIndexProperties(this object obj)
        {
            if (obj == null)
            {
                return null;
            }
            var type = obj.GetType();
            IList<MethodInfo> results = new List<MethodInfo>();

            try
            {
                var props = type.GetProperties(System.Reflection.BindingFlags.Default |
                    System.Reflection.BindingFlags.Public |
                    System.Reflection.BindingFlags.Instance);

                if (props != null)
                {
                    foreach (var prop in props)
                    {
                        var indexParameters = prop.GetIndexParameters();
                        if (indexParameters == null || indexParameters.Length == 0)
                        {
                            continue;
                        }
                        var getMethod = prop.GetGetMethod();
                        if (getMethod == null)
                        {
                            continue;
                        }
                        results.Add(getMethod);
                    }
                }

            }
            catch (Exception ex)
            {
            }

            return results;
        }
        public static object GetDefaultValue(this Type @this)
        {
            if (@this.IsValueType)
                return Activator.CreateInstance(@this);

            return null;
        }
        public static object BuildDelegate(MethodInfo method)
        {
            Type type;
            var paramL = method.GetParameters().Length;
            if (paramL > 0)
            {
                var lol = method.ToString().Split(new string[] { "(", ")" }, StringSplitOptions.None)[1];
                if (method.ReturnType == typeof(void))
                    type = Type.GetType($"System.Action`{paramL}[{lol}]");
                else
                    type = Type.GetType($"System.Func`{paramL + 1}[{lol}, {method.ReturnType.FullName}]");
            }
            else
            {
                if (method.ReturnType == typeof(void))
                    type = typeof(Action);
                else
                    type = Type.GetType($"System.Func`1[{method.ReturnType.FullName}]");
            }
            return Delegate.CreateDelegate(type, method);
        }
        public static Type MakeGeneric(string bass, params Type[] types)
        {
            var paramL = types.Length;
            return paramL > 0 ? Type.GetType($"{bass}`{paramL}[{string.Join(",", types.Select(x => x.FullName))}]") : typeof(Action);

        }
    }
}
