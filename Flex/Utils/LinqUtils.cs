﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LS.Flex.Utils
{
    public static class LinqUtils
    {
        public static IEnumerable<T> Each<T>(this IEnumerable<T> @this, Action<T> @do) => @this.Select(x => { @do(x); return x; });
        public static IEnumerable<T> Distinct<T>(this IEnumerable<T> @this, Func<T, object> comparer) where T : class => @this.Distinct(new LambdaEqualityComparer<T>(comparer));
        public class LambdaEqualityComparer<T> : IEqualityComparer<T> where T : class
        {
            private readonly Func<T, object> lambda;

            public LambdaEqualityComparer(Func<T, object> lambda)
            {
                this.lambda = lambda ?? throw new ArgumentNullException(nameof(lambda));
            }

            public bool Equals(T x, T y) => lambda(x).Equals(lambda(y));

            public int GetHashCode(T obj) => lambda(obj).GetHashCode();
        }
    }
}
